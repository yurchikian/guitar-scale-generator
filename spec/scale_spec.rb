require './app.rb'

RSpec.describe Scale do
  describe 'generator' do
    it 'generates the scale correctly' do
      scale = Scale::generate_scale('###.###.###.')
      expect(scale).to eq([:root, :note, :note, nil] +
                          [:note, :note, :note, nil] * 2)
    end

    it 'generates the scale with offset correctly' do
      scale = Scale::generate_scale('###.###.###.', 'E')
      expect(scale).to eq(([:root, :note, :note, nil] +
                        [:note, :note, :note, nil] * 2).rotate(-'CdDe'.length))
    end

    it 'generates string hits correctly' do
      scale = Scale::generate_scale('#.#.###.###.')
      hits = Scale::generate_string_hits('E', scale, 5)
      expect(hits).to eq([:note, :note, :note, nil, :note])
    end

    it 'generates scale with blues notes correctly' do
      scale = Scale::generate_scale('#..#.#b#..#.')
      expect(scale[6]).to eq(:blues)
    end
  end

  describe 'friendly cli' do
    it 'outputs friendly string graph' do
      minor_pentatonic_scale_hits = '#..#.#b#..#.'
      minor_pentatonic_scale = Scale::generate_scale(
                                              minor_pentatonic_scale_hits, 'E')

      output = Scale::print_friendly(Scale::generate_string_hits('A',
                                                    minor_pentatonic_scale, 8))

      expect(output).to eq("--O--|--o--|--O--|-----|-----|--O--|-----|--X--")
    end
  end
end
