require './app.rb'


strings = 'EBGDAE'

minor_pentatonic_scale_hits = '#..#.#b#..#.'
minor_pentatonic_scale = Scale::generate_scale( minor_pentatonic_scale_hits, 'E')


strings.each_char do |string|
  puts Scale::print_friendly(Scale::generate_string_hits(string, minor_pentatonic_scale, 19))
end

puts ' '
puts Scale::print_numbers_friendly(19)
