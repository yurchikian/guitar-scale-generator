module Scale

  def print_friendly(hits)
    hits.map { |hit|
      symbol =  case hit
                when :root
                  'X'
                when :note
                  'O'
                when :blues
                  'o'
                else
                  '-'
                end
      print_friendly_format(symbol)
    }.join('|')
  end

  def print_numbers_friendly(amount = 12)
    (0..(amount - 1)).map { |n|
      print_friendly_format(n)
    }.join('|')
  end

  def print_friendly_format(what)
    l = what.to_s.length
    p1 = (3 - l/2.0).floor
    p2 = (2 - l/2.0).ceil

    "#{'-' * p1}#{what}#{'-' * p2}"
  end

end
