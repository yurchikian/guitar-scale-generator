module Scale
  extend self

  ## Helpers
  ############################################################################


  def notes
    ## Let capital letter be the note
    ## small letter be the note b
    'CdDeEFgGaAbB'
  end

  def generate_scale(scale_hits, tone = 'C')
    offset = notes.index(tone)
    scale_hits.each_char.each_with_index.map { |hit, index|
      if notes[offset + index] == tone
        :root
      else
        recognize(hit)
      end
    }.rotate(-offset)
  end

  def recognize(hit)
    case hit
    when '#'
      :note
    when 'b'
      :blues
    end
  end

  def generate_string_hits(string, scale, amount = 12)
    string_note_index = notes.index(string)
    scale.rotate(string_note_index).cycle.take(amount)
  end
end
